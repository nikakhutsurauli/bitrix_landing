<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Loader;


Loader::includeModule('iblock');

class NewsList extends \CBitrixComponent
{

    public $arrResult;

    public function prepareParams()
    {
        return $this->arParams;
    }

    public function prepareResult()
    {

        $this->arResult["FORM_DATA"] = $_POST;
        return $this->arResult;
    }

    public function executeComponent()
    {
        $arParams = $this->prepareParams();
        $arResult = $this->prepareResult();

        if ($arParams["CACHE_TYPE"] == "Y") {
            if ($this->StartResultCache($arParams["CACHE_TIME"])) {
                $this->includeComponentTemplate();
            }
        } else {
            $this->includeComponentTemplate();
        }
    }

    public function filter($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function email_validation($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }

    public function feedback(){
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $arErrors  = [];
            $arData = [];

            foreach ($_POST as $key => $value){

                if($key == 'name'){
                    if (empty($value)) {
                        $arErrors [] = 'Name is required';
                    }
                    elseif(!preg_match('/^\pL+$/u',$value)){
                        $arErrors [] = 'Incorrect format name';
                    }
                    else{
                        $arData[$key] = $value;
                    }
                }

                if ($key == 'email'){
                    if($this->email_validation($value)){
                        $arData[$key] = $value;
                    }
                    else{
                        $arErrors[] = 'Email is required';
                    }
                }
                if ($key == 'phone'){
                    if(empty($value)){
                        $arErrors [] = 'Phone is required';
                    }
                    elseif (preg_match('/^[0-9]*$/',$value)){
                        $arErrors [] = 'Incorrect format phone';
                    }
                    else{
                        $arData[$key] = $value;
                    }
                }

                if ($key == 'date'){
                    if(empty($value)){
                        $arErrors [] = 'Data is required';
                    }
                    elseif (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$value)){
                        $arErrors [] = 'Incorrect format date';
                    }
                    else{
                        $arData[$key] = $value;
                    }
                }

                if ($key == 'message'){
                    if(empty($value)){
                        $arErrors [] = 'message is required';
                    }
                    else{
                        $arData[$key] = $value;
                    }
                }


            }

            if (count($arErrors) > 0){
                $html = '<p>';
                foreach ($arErrors as $error){
                    $html .=$error.'<br>';
                }
                $html .='</p>';
                $this->arrResult = array(
                    'status' => 'error',
                    'errors' => $html
                );
            }
            if(count($arErrors) == 0){
                $to = "nikakhutsurauli93@gmail.com";
                $from = $this->filter($_POST['email']);
                $subject = "Feedback";
                $message = $this->filter($_POST['message']);

                $headers  = "From: $from" . "\r\n" .
                    "Reply-To: $from" . "\r\n" .
                    "X-Mailer: PHP/" . phpversion();

                if (mail($to, $subject , $message, $headers)){
                    echo 'message sent';
                }
            }
        }
    }
}


