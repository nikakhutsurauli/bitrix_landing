<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if (!empty($arResult)) : ?>
<ul class="navbar-nav mx-auto">

    <? foreach ($arResult as $key => $item) : ?>
            <?
            if ($key == 0) {
                $active = ' active';
            }
            else{
                $active = '';
            }
            ?>
            <?
             if ($key == 3){
                 $sActive = 'navbar-brand d-none d-lg-block';
             }
             else{
                 $sActive = 'nav-link';
             }
            ?>
            <?
             if ($key == 3){
                 $strong = '<strong class="d-block">Health Specialist</strong>';
             }
             else{
                 $strong = '';
             }
            ?>

            <li class="nav-item <?=$active?>">
                    <a class="<?=$sActive?>" href="<?=$item["LINK"]?>"><?=$item["TEXT"]?>
                          <?=$strong?>
                    </a>
            </li>
    <? endforeach; ?>


</ul>
<? endif; ?>

