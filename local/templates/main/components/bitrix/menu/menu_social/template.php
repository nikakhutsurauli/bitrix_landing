<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <ul class="social-icon">
        <? foreach($arResult as $key => $arItem): ?>
            <?if (count($key)>0) :?>
            <li><a href="<?=$arItem["LINK"]?>" class="<?=$arItem["TEXT"]?>"></a></li>
            <? endif; ?>
        <?endforeach?>
    </ul>
<?endif?>
