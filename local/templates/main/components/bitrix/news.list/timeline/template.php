<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section-padding pb-0" id="timeline">
    <div class="container">
        <div class="row">
            <h2 class="text-center mb-lg-5 mb-4">Our Timeline</h2>
            <? if (!empty($arResult["ITEMS"])) : ?>
                <div class="timeline">
                    <? foreach ($arResult["ITEMS"] as $arItem) : ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                    <div class="row g-0 justify-content-end justify-content-md-around align-items-start timeline-nodes" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="col-9 col-md-5 me-md-4 me-lg-0 order-3 order-md-1 timeline-content bg-white shadow-lg">
                            <h3 class=" text-light"><?=$arItem["NAME"];?></h3>
                            <p><?=$arItem["PREVIEW_TEXT"];?></p>
                        </div>
                        <div class="col-3 col-sm-1 order-2 timeline-icons text-md-center">
                            <?=$arItem["DETAIL_TEXT"];?>
                        </div>
                        <div class="col-9 col-md-5 ps-md-3 ps-lg-0 order-1 order-md-3 py-4 timeline-date">
                            <?=$arItem["PROPERTIES"]["TIME"]["VALUE"];?>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
    </div>
</section>
