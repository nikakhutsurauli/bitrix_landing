<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<footer class="site-footer section-padding" id="contact">
    <div class="container">
        <div class="row">
            <? if (count($arResult["ITEMS"]) > 0) : ?>
            <? foreach ($arResult["ITEMS"] as    $arItem) :?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
            <div class="col-lg-5 me-auto col-12" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                  <h5 class="mb-lg-4 mb-3"><?=$arItem["NAME"]?></h5>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex">
                           <?=$arItem["PROPERTIES"]["CLOSED"]["VALUE"]?>
                        </li>

                        <li class="list-group-item d-flex">
                            <?=$arItem["PROPERTIES"]["DATAWORKTIMETEXT"]["VALUE"]?>
                            <span>
                                <?=$arItem["PROPERTIES"]["DATAWORKTIME"]["VALUE"] = $DB->DateFormatToPHP("08:00 AM")?>
                                -
                                <?=$arItem["PROPERTIES"]["DATATIMECLOSE"]["VALUE"] = $DB->DateFormatToPHP("03:30 PM")?>
                            </span>
                        </li>
                        <li class="list-group-item d-flex">
                            <?=$arItem["PROPERTIES"]["DATACHANGETIMETEXT"]["VALUE"]?>
                            <span>
                                <?=$arItem["PROPERTIES"]["DATACHANGETIME"]["VALUE"] = $DB->DateFormatToPHP("10:30 AM")?>
                                -
                                <?=$arItem["PROPERTIES"]["DATACHANGETIMECLOSE"]["VALUE"] = $DB->DateFormatToPHP("05:30 PM")?>
                            </span>
                        </li>
                    </ul>
                </div>
                <? endforeach; ?>
            <? endif; ?>
            <div class="col-lg-2 col-md-6 col-12 my-4 my-lg-0">
                <h5 class="mb-lg-4 mb-3">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/footer_our_clinic.php", Array(), Array("MODE" => "html"));?>
                </h5>
                <p>
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/footer_company_mail.php", Array(), Array("MODE" => "html"));?>
                <p>
                <p>
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/footer_address.php", Array(), Array("MODE" => "html"));?>
                </p>
            </div>

            <div class="col-lg-3 col-md-6 col-12 ms-auto">
                <h5 class="mb-lg-4 mb-3">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/socialtext.php", Array(), Array("MODE" => "html"));?>
                </h5>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_social", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "socialbottom",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>



            </div>

            <div class="col-lg-3 col-12 ms-auto mt-4 mt-lg-0">
                <p class="copyright-text">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/copyright.php", Array(), Array("MODE" => "html"));?>
                </p>
            </div>
        </div>
    </div>
</footer>
