<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="hero" id="hero">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php if (!empty($arResult["ITEMS"])) : ?>
                    <div id="myCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <?foreach ($arResult["ITEMS"] as $key => $arItem) :?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <?
                                  if ($key == 0){
                                      $active = ' active';
                                  }
                                  else{
                                      $active = '';
                                  }
                                ?>
                                <div class="carousel-item<?=$active?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                    <? if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) : ?>
                                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" class="img-fluid" alt="">
                                    <? endif; ?>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="heroText d-flex flex-column justify-content-center">
                        <? if (!empty($arResult["ITEMS"])) :?>
                        <div class="animated-info">
                            <?=$arItem["NAME"];?>
                            <h1 class="mt-auto mb-2">
                                <? foreach ($arResult["ITEMS"] as $ITEM) : ?>
                                        <span class="animated-item">
                                            <?=$ITEM["PROPERTIES"]["SLOGANS"]["VALUE"][0];?>
                                        </span>
                                <? endforeach; ?>
                            </h1>
                        </div>
                        <? endif; ?>
                        <p class="mb-4">
                            <?=$ITEM["PREVIEW_TEXT"];?>
                        </p>
                        <div class="heroLinks d-flex flex-wrap align-items-center">
                            <?if (!empty($ITEM["DETAIL_TEXT"]) && !empty($ITEM["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                                <a class="custom-link me-4" href="<?=$ITEM["PROPERTIES"]["LINK"]["VALUE"]?>" data-hover="Learn More">
                                    <?= $ITEM["DETAIL_TEXT"];?>
                                </a>
                            <? endif; ?>
                            <p class="contact-phone mb-0"><i class="bi-phone"></i> 010-020-0340</p>
                        </div>
                    </div>
                   <? endif; ?>
            </div>
        </div>
    </div>
</section>
