<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section-padding pb-0" id="reviews">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <? if (count($arResult["ITEMS"]) > 0) : ?>
                <h2 class="text-center mb-lg-5 mb-4">Our Patients</h2>
                <div class="owl-carousel reviews-carousel">
                     <? foreach ($arResult["ITEMS"] as $arItem) : ?>
                         <?
                         $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                         $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                         ?>
                     <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <figure class="reviews-thumb d-flex flex-wrap align-items-center rounded">
                            <div class="reviews-stars">
                                <i class="bi-star-fill"></i>
                                <i class="bi-star-fill"></i>
                                <i class="bi-star-fill"></i>
                                <i class="bi-star"></i>
                                <i class="bi-star"></i>
                            </div>
                            <p class="text-primary d-block mt-2 mb-0 w-100"><strong><?=$arItem["NAME"];?></strong></p>
                            <p class="reviews-text w-100">
                                <?=$arItem["PREVIEW_TEXT"];?>
                            </p>
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" class="img-fluid reviews-image" alt=""/>
                            <figcaption class="ms-4">
                                <strong><?=$arItem["PROPERTIES"]["NAMES"]["VALUE"];?></strong>
                                <span class="text-muted"><?=$arItem["DETAIL_TEXT"];?></span>
                            </figcaption>
                        </figure>
                     </div>
                 <? endforeach; ?>
                </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</section>

