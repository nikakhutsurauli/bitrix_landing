<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section-padding" id="about">
    <div class="container">
        <div class="row">
        <? if (!empty($arResult["ITEMS"])) : ?>
            <? foreach ($arResult["ITEMS"] as $arItem) : ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
            <div class="col-lg-6 col-md-6 col-12" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

                <h2 class="mb-lg-3 mb-3"><?=$arItem["NAME"];?></h2>
                <p><?=$arItem["PREVIEW_TEXT"];?></p>

            </div>
            <? endforeach; ?>
        <? endif; ?>
            <div class="col-lg-4 col-md-5 col-12 mx-auto">
                <div class="featured-circle bg-white shadow-lg d-flex justify-content-center align-items-center">
                    <p class="featured-text"><span class="featured-number">12</span> Years<br> of Experiences</p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
//echo '<pre>';
//print_r($Item);
//echo '</pre>';